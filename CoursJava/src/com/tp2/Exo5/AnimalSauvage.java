package com.tp2.Exo5;

public abstract class AnimalSauvage extends Animal {
	
	public void deplacement() {
		System.out.println("je me deplace dans la nature");
	}
	
	@Override
	public String toString() {
		return super.toString()+".Je suis un Animal sauvage";
	}


}
