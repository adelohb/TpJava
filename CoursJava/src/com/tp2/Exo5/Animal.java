package com.tp2.Exo5;

public abstract class Animal {

	protected double poids ;
	protected String couleur ;
	
	abstract void deplacement();
	abstract void moyenExpression() ;
	
	
	@Override
	public String toString() {
		return "je suis un Animal ";
	}
}  
