package com.tp2.Exo4;

public class Capitale extends Ville {

	private String nomPays ;

	public Capitale(String nom,String nomPays, int nbHabitants) {
		super(nom, nbHabitants);
		// TODO Auto-generated constructor stub
		this.nomPays = nomPays.toUpperCase();
		
	}

	public Capitale(String nom, String nomPays) {
		super(nom);
		// TODO Auto-generated constructor stub
		this.nomPays = nomPays.toUpperCase();
		
	}

	public String getNomPays() {
		return nomPays;
	}
	
	public void getCategorie() {
		System.out.println("catégorie de la ville de "+this.nom+" : C");
		}

	@Override
	public String toString() {
		
		return super.toString()+"\t Capitale de "+this.getNomPays() ;
	}

}
