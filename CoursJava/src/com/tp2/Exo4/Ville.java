package com.tp2.Exo4;

public class Ville {
	protected String nom ;
	protected int nbHabitants;
	public Ville(String nom) {
		String ville = nom.substring(0,1).toUpperCase()+nom.substring(1, nom.length()).toLowerCase() ;
		this.nom = ville;
		this.nbHabitants=0;
	}
	public Ville(String nom, int nbHabitants) {
		String ville = nom.substring(0,1).toUpperCase()+nom.substring(1, nom.length()).toLowerCase() ;
		this.nom = ville;
		this.nbHabitants = nbHabitants;
	}
	public String getNom() {
		return nom;
	}
	public int getNbHabitants() {
		return nbHabitants;
	}
	public void setNbHabitants(int nbHabitants) {
		if(nbHabitants>=0) {
		this.nbHabitants = nbHabitants;
		}else {
			this.nbHabitants=0 ;
		}
	}
	public boolean nbHabitantsConnu() {
		return ((this.nbHabitants>0)? true : false);
	}
	public void getCategorie() {
	if(this.nbHabitants>0 && this.nbHabitants<500000 ) {
		System.out.println("catégorie de la ville de "+this.nom+" : A");
	}else if (this.nbHabitants>500000 ) {
		System.out.println("catégorie de la ville de "+this.nom+" : B");
	}else {
		System.out.println("catégorie de la ville de "+this.nom+" : ?");
	}
	}
	@Override
	public String toString() {
		
		return "Ville de "+this.nom+" ;\t"+this.nbHabitants+" habitants" ;
	}
	
	
}
