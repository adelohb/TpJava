package com.tp2.Exo2;

public class MatriceCarree {
	private int nbrLigneCol ;
	private double[][] matrice ;
	
	public MatriceCarree() {
		super();
		// TODO Auto-generated constructor stub
	}
	public MatriceCarree(int nbrLigneCol) {
		super();
		this.nbrLigneCol = nbrLigneCol;
		
	}
	
	public MatriceCarree(double[][] matrice) {
		super();
		this.matrice = matrice;
	}
	public int getNbrLigneCol() {
		return nbrLigneCol;
	}
	public void setNbrLigneCol(int nbrLigneCol) {
		this.nbrLigneCol = nbrLigneCol;
	}
	public double[][] getMatrice() {
		return matrice;
	}
	public void setMatrice(double[][] matrice) {
		this.matrice = matrice;
	}
	public double elementAt(int i, int j) {
		
		return this.matrice[i][j] ;
	}
	public void setElementAt(double x, int i, int j) {
		this.matrice[i][j]= x ;
		
	}
	
	public double[][] add(double[][] x) {
		int rows = this.matrice.length;
	    int columns = this.matrice[0].length;
	    double[][] result = new double[rows][columns];
	    for (int i = 0; i < rows; i++) {
	      for (int j = 0; j < columns; j++) {
	        result[i][j] = this.matrice[i][j] + x[i][j];
	      }
	    }

	    return result;
		
	}
	public double[][] substract(double[][] x) {
		int rows = this.matrice.length;
	    int columns = this.matrice[0].length;
	    double[][] result = new double[rows][columns];
	    for (int i = 0; i < rows; i++) {
	      for (int j = 0; j < columns; j++) {
	        result[i][j] = this.matrice[i][j] - x[i][j];
	      }
	    }

	    return result;
		
	}
	
	public static double unite() {
		return (Double) null;
	}
	
	
	public void tostring () {
		
		for (int i = 0; i < this.matrice.length; i++) {
		    for (int j = 0; j < this.matrice[i].length; j++) {
		        System.out.print(this.matrice[i][j] + " ");
		    }
		    System.out.println();
		}
	}
	

}
