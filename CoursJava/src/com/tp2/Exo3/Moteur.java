package com.tp2.Exo3;

import java.util.Objects;

public class Moteur {
	private int nbrChevaux ;

	public Moteur() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Moteur(int nbrChevaux) {
		super();
		this.nbrChevaux = nbrChevaux;
	}

	public int getNbrChevaux() {
		return nbrChevaux;
	}

	public void setNbrChevaux(int nbrChevaux) {
		this.nbrChevaux = nbrChevaux;
	}
	
	@Override
	 public int hashCode(){
	return Objects.hash( this.nbrChevaux);
	}
	@Override
	public boolean equals(Object obj) {
		if(this == obj){
			return true;
			 }
			 if(obj.getClass() != this.getClass()){
			return false;
			}
			Moteur lo = (Moteur)obj;
			return (this.nbrChevaux == lo.getNbrChevaux());
	}
}
