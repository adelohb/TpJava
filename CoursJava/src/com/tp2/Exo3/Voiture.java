package com.tp2.Exo3;

import java.util.Objects;

public class Voiture {

	private Moteur moteur ;
	private String marque ;
	public Voiture() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Voiture(Moteur moteur, String marque) {
		super();
		this.moteur = moteur;
		this.marque = marque;
	}
	public Moteur getMoteur() {
		return moteur;
	}
	public void setMoteur(Moteur moteur) {
		this.moteur = moteur;
	}
	public String getMarque() {
		return marque;
	}
	public void setMarque(String marque) {
		this.marque = marque;
	}
	
	@Override
	public String toString(){
		
		return "moteur :"+this.moteur.getNbrChevaux()+"Chevaux  marque :"+marque ;
	}
	@Override
	 public int hashCode(){
	return Objects.hash(this.marque.hashCode(), this.moteur);
	}
	@Override
	public boolean equals(Object obj) {
		if(this == obj){
			return true;
			 }
			 if(obj.getClass() != this.getClass()){
			return false;
			}
			 Voiture lo = (Voiture)obj;
			return  this.moteur.equals(lo.getMoteur()) && (this.marque == lo.getMarque());
	}
}
