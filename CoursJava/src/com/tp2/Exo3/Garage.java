package com.tp2.Exo3;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Garage {
	
	private List<Voiture> enReparation = new ArrayList<Voiture>() ;
	
	
	
	public Garage() {
		super();
		// TODO Auto-generated constructor stub
	}



	public Garage(List<Voiture> enReparation) {
		super();
		this.enReparation = enReparation;
	}



	public List<Voiture> getEnReparation() {
		return enReparation;
	}



	public void setEnReparation(List<Voiture> enReparation) {
		this.enReparation = enReparation;
	}



	public void upgrade(Voiture v, int augmentationPuissance) {	
		if (this.enReparation.contains(v)) {
			System.out.println("trouver");
			for( int i=0; i<this.enReparation.size() ; i++) {
				if(this.enReparation.get(i).equals(v)) {
					
					this.enReparation.get(i).getMoteur().setNbrChevaux(this.enReparation.get(i).getMoteur().getNbrChevaux()+augmentationPuissance);
				}
			}
		}else {
			System.out.println("pas trouver");

		}
	}
	
}
