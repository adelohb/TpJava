package com.tp2.exo7;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;

public abstract class GeometricPrimitive {
	
	private List<Coordinate> coords = new ArrayList<Coordinate>();
	private Color color;
	
	public abstract void draw(Graphics g);
	
	public void translate(int x, int y) {
		for (int i=0;i<coords.size();i++) {
			coords.get(i).setX(x+coords.get(i).getX());
			coords.get(i).setY(y+coords.get(i).getY());

			
		}
	}
	
	public  Coordinate centroid() {
		double centroidX=0, centroidY=0;
		for(Coordinate knot : coords) {
            centroidX += knot.getX();
            centroidY += knot.getY();
        }
    return new Coordinate(centroidX / coords.size(), centroidY / coords.size());
		
	}
	public GeometricPrimitive() {
		super();
		// TODO Auto-generated constructor stub
	}
	public List<Coordinate> getCoords() {
		return coords;
	}
	
	public Color getColor() {
		return color;
	}
	public void setColor(Color color) {
		this.color = color;
	}
	
	

}
