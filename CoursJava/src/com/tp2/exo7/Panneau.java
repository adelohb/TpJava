package com.tp2.exo7;

import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

public class Panneau extends JPanel {
	
	GeometricPrimitive geom ;
	public void paintComponent(Graphics g ){
	     
	    geom.draw(g);
	  }
	public Panneau (GeometricPrimitive geom) {
		this.geom=geom;
	  
	}
}
