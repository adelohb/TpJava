package com.tp2.exo7;

public class Coordinate {
	
	private double x;
	private double y;
	public double getX() {
		return x;
	}
	public void setX(double d) {
		this.x = d;
	}
	public double getY() {
		return y;
	}
	public void setY(double y) {
		this.y = y;
	}
	public Coordinate(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}
	
	
	

}
