package com.tp2.exo7;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class Line extends GeometricPrimitive {
	private Coordinate p1 ;
	private Coordinate p2 ;
	
	public Line(Coordinate x1 , Coordinate x2 , Color color) {
		super();
		// TODO Auto-generated constructor stub
		p1=x1;
		p2=x2;
		this.getCoords().add(x1);
		this.getCoords().add(x2);
		this.setColor(color);
	}
	public Coordinate first() {
		return this.getCoords().get(0);
	}
	public Coordinate last() {
		
		return this.getCoords().get(this.getCoords().size()-1);
	}
	public Coordinate middle() {
		
		return new Coordinate((this.first().getX()+this.last().getX())/2 ,(this.first().getY()+this.last().getY())/2 );
	}
	@Override
	public void draw(Graphics  graphics2d) {
		// TODO Auto-generated method stub
		graphics2d.drawLine((int)p1.getX(),(int) p1.getY(),(int) p2.getX(),(int) p2.getY());
		graphics2d.setColor(this.getColor());
	}
	

}
