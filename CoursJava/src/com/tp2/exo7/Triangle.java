package com.tp2.exo7;

import java.awt.Graphics;
import java.awt.Graphics2D;

public class Triangle extends GeometricPrimitive implements Surfaceable{
	private int hauteur ;
	private int largeur ;
	
	public Triangle(Coordinate coord , int heuteur, int largeur) {
		this.hauteur=heuteur;
		this.largeur=largeur;
		this.getCoords().add(coord);
		Coordinate point2 =new Coordinate(coord.getX()+largeur, coord.getY());
		this.getCoords().add(point2);
		Coordinate pointInt = new Coordinate((coord.getX()+point2.getX())/2, (coord.getY()+point2.getY())/2);
		Coordinate point3 = new Coordinate(pointInt.getX(), pointInt.getY()+hauteur);
		this.getCoords().add(point3);
		
		 
	}

	@Override
	public double surface() {
		// TODO Auto-generated method stub
		return hauteur*largeur/2;
	}

	@Override
	public void draw(Graphics graphics2d) {
		// TODO Auto-generated method stub
		int[] x= new int[this.getCoords().size()];
		int[] y= new int[this.getCoords().size()];

		for(int i=0 ; i<this.getCoords().size() ; i++) {
			x[i]=(int) this.getCoords().get(i).getX();
			y[i]=(int) this.getCoords().get(i).getY();

		}
		graphics2d.drawPolygon(x,y,this.getCoords().size());
	}
	

}
