package com.tp2.exo7;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class Rectangle extends GeometricPrimitive implements Surfaceable {

	private double hauteur;
	private double largeur;
	public Rectangle(Coordinate coord,Color color ,double h, double l) {
		Coordinate p2 = new Coordinate(coord.getX(), coord.getY()+h);
		Coordinate p3 = new Coordinate(p2.getX()+l, p2.getY());
		Coordinate p4=  new Coordinate(p3.getX(), p3.getY()+h);
		this.getCoords().add(coord);
		this.getCoords().add(p2);
		this.getCoords().add(p3);
		this.getCoords().add(p4);
		this.setColor(color);
		this.hauteur=h;
		this.largeur=l;

		
	}

	@Override
	public Coordinate centroid() {
		// TODO Auto-generated method stub
		 return new Coordinate((this.getCoords().get(0).getX()+this.getCoords().get(2).getX())/2 ,(this.getCoords().get(0).getY()+this.getCoords().get(2).getY())/2 );
	}

	@Override
	public double surface() {
		// TODO Auto-generated method stub
		return hauteur*largeur;
	}

	@Override
	public void draw(Graphics graphics2d) {
		// TODO Auto-generated method stub
		graphics2d.setColor(this.getColor());
		graphics2d.drawRect((int)this.getCoords().get(0).getX(),(int) this.getCoords().get(0).getY(), (int) this.largeur,(int) this.hauteur);
	}
	
	

}
