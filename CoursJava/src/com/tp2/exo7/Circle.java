package com.tp2.exo7;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class Circle extends Point implements Surfaceable {

	private int radius ;
	public Circle(Coordinate coord ,Color color, int radius) {
		super(coord);
		// TODO Auto-generated constructor stub
		this.radius=radius ;
		this.setColor(color);
	}
	@Override
	public double surface() {
		// TODO Auto-generated method stub
		return 3.14*radius*radius;
	}
	@Override
	public void draw(Graphics graphics2d) {
		// TODO Auto-generated method stub
		graphics2d.setColor(this.getColor());
		graphics2d.drawOval((int)this.getCoords().get(0).getX(),(int) this.getCoords().get(0).getY(),(int) radius,(int) radius);
		
	}
}
