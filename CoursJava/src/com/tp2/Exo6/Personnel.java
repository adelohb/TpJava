package com.tp2.Exo6;

import java.util.ArrayList;
import java.util.List;
/**
 * 
 * @author adelohb
 *
 */
public class Personnel {

	
	List<Employe> listEmploye = new ArrayList<Employe>();
/**
 * 
 * @return la liste des employes
 */
	public List<Employe> getListEmploye() {
		return listEmploye;
	}
 /**
  * 
  * @param listEmploye
  */
	public void setListEmploye(List<Employe> listEmploye) {
		this.listEmploye = listEmploye;
	}
	/**
	 * 
	 * @param e  ajouter un employe a la liste
	 */
	public void ajouterEmploye(Employe e) {
		this.listEmploye.add(e);
	}
	/**
	 * affiche tous les salaire des employes
	 */
	public void afficherSalaires() {
		for(Employe e : this.listEmploye) {
			System.out.println(e.getNom() +" touche :"+e.calculerSalaire());
		}
	}
	/**
	 * 
	 * @return la salaire moyen
	 */
	public double salaireMoyen() {
		double somme=0 ;
		for(Employe e : this.listEmploye) {
			somme+=e.calculerSalaire();
		}
		return somme/this.listEmploye.size();
	}
}
