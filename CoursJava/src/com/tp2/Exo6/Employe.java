package com.tp2.Exo6;
/**
 * 
 * @author adelohb
 *
 */
public abstract class Employe {
	protected String nom ;
	protected String prenom ;
	protected int age ;
	protected String dateEntreService ;
	/**
	 * 
	 * @return calcule le salaire d'un employe
	 */
	public abstract double calculerSalaire();
	/**
	 * 
	 * @return renvoie le nom et le prenom de l'employe
	 */
	public String getNom() {
		return "L'employé "+this.prenom+" "+this.nom;
	}
/**
 * 
 * @param nom : nom de l'employe
 * @param prenom : prenom de l'employe
 * @param age : age de l'employe
 * @param dateEntreService : date d'entree en service  de l'employe
 */
	public Employe(String nom, String prenom, int age, String dateEntreService) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.age = age;
		this.dateEntreService = dateEntreService;
	}
	
	
	
	
	
	

}
