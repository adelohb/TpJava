package com.tp2.Exo6;
/**
 * 
 * @author adelohb
 *
 */
public class EmployeManutention extends Employe {
	
	private int nbrHeure ;
	/** 
	 * constructeur employe manutention
	 * @param nom : nom de l'employe
	 * @param prenom : prenom de l'employe
	 * @param age : age de l'employe
	 * @param dateEntreService : date d'entree en service  de l'employe
	 * @param nbrHeure nombre d'heure travailler
	 */
	public EmployeManutention(String nom, String prenom, int age, String dateEntreService, int nbrHeure) {
		super(nom, prenom, age, dateEntreService);
		this.nbrHeure = nbrHeure;
	}

	@Override
	public double calculerSalaire() {
		// TODO Auto-generated method stub
		return this.nbrHeure*65;
	}
	@Override
	public String getNom() {
		return "Manutentioniste "+this.prenom+" "+this.nom;
	}

	
}
