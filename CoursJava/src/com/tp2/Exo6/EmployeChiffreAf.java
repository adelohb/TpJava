package com.tp2.Exo6;
/**
 * 
 * @author adelohb
 *
 */
public abstract class EmployeChiffreAf extends Employe {
	
	private int chiffreAffaire;
/** 
 * @param nom : nom de l'employe
 * @param prenom : prenom de l'employe
 * @param age : age de l'employe
 * @param dateEntreService : date d'entree en service  de l'employe
 * @param chiffreAffaire chiffre d'affaire produit mensuelement
 */
	public EmployeChiffreAf(String nom, String prenom, int age, String dateEntreService, int chiffreAffaire) {
		super(nom, prenom, age, dateEntreService);
		this.chiffreAffaire = chiffreAffaire;
	}

/**{@inheritDoc}
 * 
 */
	@Override
	public double calculerSalaire() {
		// TODO Auto-generated method stub
		
		return (double)this.chiffreAffaire * 0.2 ;

	}

}
