package com.tp2.Exo6;
/**
 * 
 * @author adelohb
 *
 */
public class EmployeRepresentation extends EmployeChiffreAf {
/**
 * @param nom : nom de l'employe
 * @param prenom : prenom de l'employe
 * @param age : age de l'employe
 * @param dateEntreService : date d'entree en service  de l'employe
 * @param chiffreAffaire chiffre d'affaire produit mensuelement
 */
	public EmployeRepresentation(String nom, String prenom, int age, String dateEntreService, int chiffreAffaire) {
		super(nom, prenom, age, dateEntreService, chiffreAffaire);
		// TODO Auto-generated constructor stub
	}
	@Override
	public double calculerSalaire() {
		return super.calculerSalaire()+800;
	}
	@Override
	public String getNom() {
		return "Representant "+this.prenom+" "+this.nom;
	}

}
