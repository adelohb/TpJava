package com.tp2.Exo6;
/**
 * 
 * @author adelohb
 *
 */
public class EmployeProduction extends Employe  {
	private int nbrUnite ;
	
/**
 * constructeur employee production
 * @param nom : nom de l'employe
 * @param prenom : prenom de l'employe
 * @param age : age de l'employe
 * @param dateEntreService : date d'entree en service  de l'employe
 * @param nbrUnite nobre d'unite produites
 */
	public EmployeProduction(String nom, String prenom, int age, String dateEntreService, int nbrUnite) {
		super(nom, prenom, age, dateEntreService);
		this.nbrUnite = nbrUnite;
	}

/**{@inheritDoc}
 * 
 */
	@Override
	public double calculerSalaire() {
		// TODO Auto-generated method stub
		return this.nbrUnite*5;
	}
	/**{@inheritDoc}
	 * 
	 */
	@Override
	public String getNom() {
		return "Producteur "+this.prenom+" "+this.nom;
	}



}
