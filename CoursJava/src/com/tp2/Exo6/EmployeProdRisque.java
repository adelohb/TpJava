package com.tp2.Exo6;
/**
 * 
 * @author adelohb
 *
 */
public class EmployeProdRisque extends EmployeProduction implements IPrime {
/**
 * @param nom : nom de l'employe
 * @param prenom : prenom de l'employe
 * @param age : age de l'employe
 * @param dateEntreService : date d'entree en service  de l'employe
 * @param nbrUnite nombre d'unité produite
 */
	public EmployeProdRisque(String nom, String prenom, int age, String dateEntreService, int nbrUnite) {
		super(nom, prenom, age, dateEntreService, nbrUnite);
		// TODO Auto-generated constructor stub
	}
	/**{@inheritDoc}
	 * 
	 */
	@Override
	public double calculerSalaire() {
		// TODO Auto-generated method stub
		return super.calculerSalaire()+prime;
	}

}
