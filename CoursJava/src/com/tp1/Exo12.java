package com.tp1;

import java.math.BigInteger;

public class Exo12 { 
	
	public BigInteger fact(BigInteger n) {
		if(n.equals(0)) {
			
			return BigInteger.valueOf(1) ;
		}else {
			return n.multiply(fact(n.subtract(BigInteger.valueOf(1))));
		}
		
	}
	
	
	public Exo12() {
		
	}
	
}
