package com.tp1;

import java.util.HashMap;
import java.util.Map;

public class Exo14 {

	public Exo14() {
		
	}
	
	public int nbrOccurence(String text , String mot) {

		String[] tab = text.split(" ") ;
		int nbr=0;
		for(String src : tab) {
			if(src.equals(mot)) {
				nbr++;
			}
		}
		
		return nbr ;
	}
	public Map<String,Integer> nbrOccurence(String text){
		
		String[] tab = text.split(" ") ;
		Map<String,Integer> map = new HashMap<String,Integer>();
		for(String src : tab) {
			if (map.containsKey(src)) {
				map.replace(src, map.get(src)+1);
			}else {
				map.put(src, 1);
			}
			
		}

		return map;
	}
}
