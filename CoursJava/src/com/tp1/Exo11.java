package com.tp1;

import java.util.Arrays;

public class Exo11 {
	public int[] tab ;
	public int maxTableau(int[] tab) {
		
		Arrays.sort(tab);
		return tab[tab.length-1] ;
	}
	public void ajoutElement(int val , int pos) {
		
		int[] tabBis = new int[tab.length+1];
		for(int i =0 ; i<tabBis.length ; i++) {
			if(i==pos) {
				tabBis[i]=val ;
			}else if (i>pos){
				tabBis[i]=tab[i-1];
			}else {
				tabBis[i]=tab[i];
			}
		}
		afficheTableau(tabBis);
		this.tab = new int[tabBis.length];
		this.tab = tabBis ;
	}
	public void afficheTableau(int[] tab) {
		System.out.print("<");
		for(int i : tab) {
			System.out.print(i+" ,");
		}
		System.out.print("> \n");
	}
	
	public int recherche(int[] tab, int val) {
		
		boolean test= true ;
		int i =0, pos =0;
		while (i<tab.length && test ) {
			if(tab[i]== val) {
				test = false ;
				pos = i ;
			}else {
				i++;
			}	
		}
		if (test==true) {
			return -1 ;
		}else {
			return pos ;
		}	
	}
	public int[] rechercherTous(int[] tab, int val) {
		
		
		String s="";
		for(int i=0 ; i<tab.length ; i++) {
			if(tab[i]== val) {
			
				s+=i+" ";
		}	
		}
		String[] stab = s.split(" ");
		int[] res = new int[stab.length];
		for(int  i=0; i<stab.length ; i++) {
			res[i]= Integer.parseInt(stab[i]);
			
		}
		return res ;
	}
	public Exo11() {
		
	}
}
