package main;


import java.awt.Color;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import com.tp1.*;
import com.tp2.Exo2.MatriceCarree;
import com.tp2.Exo3.Garage;
import com.tp2.Exo3.Moteur;
import com.tp2.Exo3.Voiture;
import com.tp2.Exo4.Capitale;
import com.tp2.Exo4.Ville;
import com.tp2.Exo5.*;
import com.tp2.Exo6.Employe;
import com.tp2.Exo6.EmployeRepresentation;
import com.tp2.Exo6.EmployeVente;
import com.tp2.exo7.*;
import com.tp2.Exo6.*;

public class Principale {



	public static void main(String[] args) {
	
	    //Tp1 tp1 = new Tp1();
		//System.out.println("pgcd :"+tp1.Pgcd(2,4));
		//Exo11 e = new Exo11();
		//int[] tab= {1,65,2,1,21,1} ;
		//System.out.println(e.maxTableau(tab));
		//e.afficheTableau(tab);
		//e.tab = tab ;
		//e.ajoutElement(1000, 2);
		//System.out.println(e.recherche(tab, 655));
		//int[] t2 = e.rechercherTous(tab, 1);
		//e.afficheTableau(t2);
//		Exo12 ex = new Exo12();
//		for(int i=1 ; i<=20 ; i++ ) {
//			System.out.print(ex.fact(BigInteger.valueOf(i))+"  ");
//		}
//		Exo14 ex = new Exo14();
//		String text ="adel bobo bibi adel alla bobo" ;
//		System.out.println(ex.nbrOccurence(text, "adel"));
//		Map<String,Integer> map = ex.nbrOccurence(text);
//		for (Map.Entry<String, Integer> entry : map.entrySet())
//		{
//		    System.out.println(entry.getKey() + " :" + entry.getValue());
//		}
//		
//		String text ="adel,bobo,bibi" ;
//		Exo15 ex = new Exo15();
//		ArrayList<String> list = ex.eclater(text, ",");
//		for(String src : list ) {
//			System.out.println(src);
//		}
//	
//		List<String> list1 = new ArrayList<String>();
//		list1.add("adell");
//		list1.add("sig");
//		for(String src : list1 ) {
//			System.out.println(src);
//		}
//		 System.out.println(list1.size());
//		 list1.remove(0);
//		 list1.clear();
//		 System.out.println(list1.size());
//		 
//		 List<Integer> list2 = new LinkedList<Integer>();
//		 list2.add(25);
//		 list2.add(36);
//		 for(int src : list2 ) {
//				System.out.println(src);
//			}
//		SortedSet<Integer> set = new TreeSet<Integer>(); 
//		set.add(25);
//		set.add(680);
//		set.add(10);
//		for(int src : set ) {
//			System.out.println(src);
//		}
		
//		double[][] mat = {{1,5,6},
//						  {11,55,6},
//						  {13,55,66}};
//		double[][] mat1 = {{1,2,3},
//						  {8,5,5},
//						  {1,7,6}};
//		MatriceCarree m = new MatriceCarree(mat);
//		m.add(mat1);
//		double[][] ajout = m.add(mat);
//		for (int i = 0; i < ajout.length; i++) {
//		    for (int j = 0; j < ajout.length; j++) {
//		        System.out.print(ajout[i][j] + " ");
//		    }
//		    System.out.println();
//		}
//		
//		Garage garage = new Garage() ;
//		List<Voiture> listV = new ArrayList<Voiture>();
//		listV.add(new Voiture(new Moteur(5), "audi"));
//		listV.add(new Voiture(new Moteur(10), "porche"));
//		listV.add(new Voiture(new Moteur(56), "Benz"));
//		listV.add(new Voiture(new Moteur(65), "BMW"));
//		garage.setEnReparation(listV);
//		
//		for ( Voiture v : garage.getEnReparation()) {
//			
//			System.out.println(v.toString());
//		}
//		
//		Random r = new Random();
//		int indx = r.nextInt(listV.size()-1) + 1;
//		
//		garage.upgrade(new Voiture(new Moteur(65), "BMW"), 300);
//		
//		for ( Voiture v : garage.getEnReparation()) {
//			
//			System.out.println(v.toString());
//		}

//			Ville v = new Ville("marseille");
//			Ville v1 = new Ville("LILLE", 20005552);
//			System.out.println(v);
//			System.out.println(v1);
//			System.out.println(" ");
//			Capitale c1, c2;
//			
//			c1 = new Capitale("Paris", "France");
//			c2 = new Capitale("Rome", "Italie", 2700000);
//			c1.setNbHabitants(2181371);
//			System.out.println(c1);
//			System.out.println(c2);
//			System.out.println(" ");
//
//			v.getCategorie();
//			v1.getCategorie();
//			c1.getCategorie();
		
//		ICarnivore chat = new Chat();
//		ICarnivore tigre = new Tigre();
//		System.out.println(chat.toString());
//		System.out.println(tigre.toString());
//		tigre.manger((Animal)chat);
//		Employe e1 = new EmployeVente("adel", "ohb", 14, "12-05-1992", 1000);
//		Employe e2 = new EmployeRepresentation("adel2", "ohb2", 14, "12-05-1992", 1000);
//		System.out.println(e1.calculerSalaire());
//		System.out.println(e2.calculerSalaire());

//		Personnel p = new Personnel();
//		p.ajouterEmploye(new EmployeVente("Pierre", "Business", 45, "1995",
//		30000));
//		p.ajouterEmploye(new EmployeRepresentation("Léon", "Vendtout", 25, "2001",
//		20000));
//		p.ajouterEmploye(new EmployeProduction("Yves", "Bosseur", 28, "1998", 1000));
//		p.ajouterEmploye(new EmployeManutention("Jeanne", "Stocketout", 32,
//		"1998", 45));
//		p.ajouterEmploye(new EmployeProdRisque("Jean", "Flippe", 28, "2000",
//		1000));
//		p.ajouterEmploye(new EmployeManuRisqu("Al", "Abordage", 30, "2001", 45));
//		p.afficherSalaires();
//		System.out.println("Le salaire moyen dans l'entreprise est de " +
//		p.salaireMoyen() + " francs.");
//		
		Coordinate coord = new Coordinate(50,600);
		Coordinate coord1 = new Coordinate(100,200);
		Coordinate coord2 = new Coordinate(150,60);
		GeometricPrimitive p = new Point(coord);
		Line p1 = new Line(coord,coord1, Color.CYAN);
		Triangle  p3 = new Triangle(coord1, 100, 200);
		Circle c = new Circle(coord2, Color.red, 50);
		   Rectangle r = new Rectangle(new Coordinate(250, 250), Color.black, 50, 90);
		   List<GeometricPrimitive> list = new ArrayList<GeometricPrimitive>();
		   list.add(p1);
		   list.add(p3);
		   list.add(c);
		   list.add(r);
		   Drawninig dr = new Drawninig(list);
		
		
		
		Fenetre f = new Fenetre(dr) ;
		
		
	}	
}
