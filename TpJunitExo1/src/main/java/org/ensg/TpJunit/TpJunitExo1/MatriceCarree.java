package org.ensg.TpJunit.TpJunitExo1;

import java.util.Arrays;
/**
 * Exercice 3 sur la POO
 * 
 * @author ENSG / DEI
 *
 */
public class MatriceCarree implements Cloneable{
	/**
	 * Nombre de lignes / colonnes
	 */
	private int size;
	private double[][] values; // tableau de nombres correspondant à la matrice


	/*
	 * Constructeurs
	 */

	public MatriceCarree(int size, double[][] values) {
		super();
		this.size = size;
		this.values = values;
	}

	public MatriceCarree() {
		super();
	}

	public MatriceCarree(int size) {
		super();
		this.size = size;
		this.values = new double[this.size][this.size];
	}

	public MatriceCarree(double[][] values) {
		super();
		this.values = values;
		this.size = values.length;
	}

	/*
	 * Acesseurs et mutateurs
	 */

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public double[][] getValues() {
		return values;
	}

	public void setValues(double[][] values) {
		this.values = values;
	}

	public double getElementAt(int i, int j) {
		return this.values[i][j];
	}

	public void setElementAt(int i, int j, double x) {
		this.values[i][j] = x;
	}

	/**
	 * Opération d'addition de deux matrices
	 * @param A
	 * @return
	 */
	public MatriceCarree plus(MatriceCarree A) {
		double[][] mat = new double[this.size][this.size];
		for(int i=0; i< this.size; i++) {
			for(int j=0; j< this.size; j++) {
				mat[i][j] = this.getElementAt(i, j) + A.getElementAt(i, j);
			}
		}
		return new MatriceCarree(mat);
	}

	/**
	 * Opération de soustraction de deux matrices
	 * @param A
	 * @return
	 */
	public MatriceCarree less(MatriceCarree A) {
		double[][] mat = new double[this.size][this.size];
		for(int i=0; i< this.size; i++) {
			for(int j=0; j< this.size; j++) {
				mat[i][j] = this.getElementAt(i, j) - A.getElementAt(i, j);
			}
		}
		return new MatriceCarree(mat);
	}

	/**
	 * Opération de multiplication de deux matrices
	 * @param A
	 * @return
	 */
	public MatriceCarree times(MatriceCarree A) {
		double[][] mat = new double[this.size][this.size];
		for(int i=0; i< this.size; i++) {
			for(int j=0; j< this.size; j++) {
				double somme = 0;
				for(int k=0; k< this.size; k++) {
					somme += this.getElementAt(i, k) * A.getElementAt(k,j);
				}
				mat[i][j] = somme;
			}
		}
		return new MatriceCarree(mat);
	}

	@Override
	public boolean equals(Object o) {
		// redéfinition de equals héritée d'Object
		if(!(o instanceof MatriceCarree)) {
			return false;
		}
		// o est bien une MatriceCarree
		MatriceCarree M = (MatriceCarree)o; // on cast !
		if(this.size != M.getSize()) {
			return false;
		}
		for(int i=0; i< this.size; i++) {
			for(int j=0; j< this.size; j++) {
				if(this.getElementAt(i, j) != M.getElementAt(i, j)) {
					return false;
				}
			}
		}
		return true;
	}
	
	/**
	 * Redéfinition de la méthode clone
	 * Il faut implémenter Cloneable !
	 */
	@Override
	public MatriceCarree clone() {
		//return new MatriceCarree(this.getValues()); // ne fonctionne pas car il faut également cloner le tableau (sinon, une copie de la référence est effectuée ...)
		double[][] mat = new double[this.size][this.size];
		// deep copy
		for(int i=0; i< this.size; i++) {
			for(int j=0; j< this.size; j++) {
				mat[i][j] = this.getElementAt(i, j);
			}
		}
		return new MatriceCarree(mat); 
	}
	
	/**
	 * Retourne la matrice de taille n ne contenant que des zéro
	 * @param n
	 * @return
	 */
	public static MatriceCarree zero(int n) {
		MatriceCarree zero = new MatriceCarree(n);
		for(int i=0; i< n; i++) {
			for(int j=0; j< n; j++) {
				zero.setElementAt(i, j, 0.);
			}
		}
		return zero;
	}
	
	/**
	 * Retourne la matrice de taille n ne contenant que des 1
	 * @param n
	 * @return
	 */
	public static MatriceCarree unity(int n) {
		MatriceCarree zero = new MatriceCarree(n);
		for(int i=0; i< n; i++) {
			for(int j=0; j< n; j++) {
				zero.setElementAt(i, j, 1.); 
			}
		}
		return zero; 
	}

	
}
