package org.ensg.TpJunit.TpJunitExo1;

import static org.junit.Assert.*;

import org.junit.Test;

public class MatriceCarreeTest {
	 int size = 2;
	 double[][] values= {{0,0},{0,0}} ;
	 MatriceCarree matrice = new MatriceCarree(size, values);

	
	@Test
	public void testMatriceCarree() {
		MatriceCarree mat = new MatriceCarree() ;
	}



	@Test
	public void testGetSize() {
      
		assertEquals(2, this.matrice.getSize());
	}

	@Test
	public void testSetSize() {
		
		MatriceCarree mat = new MatriceCarree() ; 
		mat.setSize(5);
		assertEquals(5, mat.getSize());
		
	}

	@Test
	public void testGetValues() {
		double tab[][] = {{5,2},{6,3}};
		MatriceCarree mat = new MatriceCarree(tab);
		assertTrue(tab.equals(mat.getValues()));
	}

	@Test
	public void testSetValues() {
		double tab[][] = {{5,2},{6,3}};
		MatriceCarree mat = new MatriceCarree();
		mat.setValues(tab);
		assertTrue(tab.equals(mat.getValues()));
		
	}

	@Test
	public void testGetElementAt() {
		double tab[][] = {{5.,2.},{6.,3.}};
		MatriceCarree mat = new MatriceCarree(tab);
		int i=0, j=0;
		assertEquals(5.0, mat.getElementAt(i, j),0);
 	//assertTrue(5== mat.getElementAt(i, j));
	}
//
//	@Test
//	public void testSetElementAt() {
//		fail("Not yet implemented");
//	}
//
	@Test
	public void testPlus() {
		double tab[][] = {{5,2},{6,3}};
		MatriceCarree mat = new MatriceCarree(tab);
	    assertTrue(mat.equals(mat.plus(matrice)));

	
	}

	@Test
	public void testLess() {
		double tab[][] = {{5,2},{6,3}};
		MatriceCarree mat = new MatriceCarree(tab);
	    assertTrue(mat.equals(mat.less(matrice)));
	
	}

	@Test
	public void testTimes() {
		double tab[][] = {{5,2},{6,3}};
		MatriceCarree mat = new MatriceCarree(tab);
	    assertTrue(matrice.equals(matrice.times(mat)));	}

	@Test
	public void testEqualsObject() {
		double tab[][] = {{1,0},{5,0}};
		MatriceCarree mat = new MatriceCarree(tab);
		assertFalse(mat.equals(matrice));

		
		double tab1[][] = {{0,0},{0,0}};
		 mat = new MatriceCarree(tab1);
		assertTrue(mat.equals(matrice));
		
		double tab2[][] = {{1,0}};
		 mat = new MatriceCarree(tab2);
		assertFalse(mat.equals(matrice));
		
		
		assertFalse(mat.equals(new Object()));
		
		
	}

	@Test
	public void testClone() {
		double tab1[][] = {{0,0},{0,0}};
		MatriceCarree mat = new MatriceCarree(tab1);
		assertTrue(mat.equals(matrice.clone()));
		
	}

	@Test
	public void testZero() {
		double tab1[][] = {{4,5},{2,0}};
		MatriceCarree mat = new MatriceCarree(tab1);
		assertTrue(matrice.equals(mat.zero(size)));
		
	}

	@Test
	public void testUnity() {
		double tab1[][] = {{1,1},{1,1}};
		MatriceCarree mat = new MatriceCarree(tab1);
		assertTrue(mat.equals(matrice.unity(size))); 
	}

}
