package org.test.BonCoin;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.AssertJUnit;
import java.util.concurrent.TimeUnit;



import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Tests {
	private  WebDriver  driver;
	private BonCoinPage bonCoinPage ;
	private LoginPage loginPage ;
	
	@BeforeClass(alwaysRun = true)
	   public  void setUp() {

	      driver = new FirefoxDriver();
	      driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	   }
	@BeforeMethod(alwaysRun = true)
	   public void openHomePage() {
	      bonCoinPage = new BonCoinPage(driver).open();
	   }
	
	@AfterClass(alwaysRun = true)
	public  void tearDown() {
		driver.quit();
	}


	@Test
	public void testConnect() {
	   // driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		bonCoinPage= new BonCoinPage(driver);
		loginPage = new LoginPage(driver);
		String tittle = loginPage.getLoginPageTittle() ;
		AssertJUnit.assertTrue(tittle.contains("/home"));
		bonCoinPage  = loginPage.loginAs("adel", "ohb");
		AssertJUnit.assertTrue(bonCoinPage.getBoncoinUrl().equals("http://www.leboncoin.fr"));
		
		
	 }

}
