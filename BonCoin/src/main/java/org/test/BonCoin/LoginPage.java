package org.test.BonCoin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.FindBy;

public class LoginPage {

	private static final String LOGIN_PAGE_URL = "/home";
	
	private static final By USERNAME =By.name("username")  ;
	private static final By PASSWORD= By.name("password");
	private static final By CONNECT= By.name("signIn");
	private WebDriver webDriver ;
	
	   public LoginPage(WebDriver driver) {
		   
		   this.webDriver = driver ;
	   }
	   public LoginPage open() {
		   webDriver.get(LOGIN_PAGE_URL);
		   return this; 
	   }
	   public BonCoinPage loginAs(String username, String password) {
		   
		   webDriver.findElement(PASSWORD).sendKeys(password);
		   webDriver.findElement(USERNAME).sendKeys(username);
		   webDriver.findElement(CONNECT).click(); 
		  return new BonCoinPage(webDriver) ;   
		   
	   }
	   public String getLoginPageTittle() {
		   return LOGIN_PAGE_URL ;
	   }
	    

	
	
}
