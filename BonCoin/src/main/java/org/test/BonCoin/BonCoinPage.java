package org.test.BonCoin;

import org.openqa.selenium.WebDriver;

public class BonCoinPage {
	
	private static final String BON_COIN_PAGE = "http://www.leboncoin.fr";
	private WebDriver webDriver;
	
	   public BonCoinPage(WebDriver driver) {
		   this.webDriver=driver ;
		   
	   }
 
	   public BonCoinPage open() {
		   webDriver.get(BON_COIN_PAGE);
		   return this;
	   }
	   
	   public String getBoncoinUrl() {
		   return BON_COIN_PAGE ;
	   }
	


}
